import { Link } from "react-router-dom";
import { useState } from 'react';

export default function Navbar(props) {
  let [title, setTitle] = useState("Saurabh");
  function titleHandler(event) {
    event.preventDefault();
    var value = document.getElementById('titleid').value;
    setTitle(value);
  }
  return (
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container-fluid">
        <Link to="/" class="navbar-brand" href="#">{title}</Link>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            <form class="d-flex">
              <input class="form-control me-2" onChange={titleHandler} id="titleid" type="search" placeholder="Search" aria-label="Title" />
              <Link to="/search"><button  class="btn btn-outline-success" type="submit">Set Title</button></Link>
            </form>

          </ul>
          <form class="d-flex">
            {true && <Link to="/login"><button class="btn btn-outline-success" type="submit">Login</button></Link>}
            {true && <Link to="/signup"><button class="btn btn-outline-success" type="submit">SingUp</button></Link>}
            {false && <button class="btn btn-outline-success" type="submit">Logout</button>}
          </form>
        </div>
      </div>
    </nav>
  )
}