import data from './cakesdata';
import Cake from "./Cake";
import axios from 'axios';
import {apiURL} from './ApiConstantUrl'
import { useState, useEffect } from "react";


let Saerch = () => {
    let [searchString, setSearch] = useState();
    let [cackesData, setingCakesData] = useState(data);

    let allCakesData = [];

    axios({
        url: apiURL.GETALLCAKES,
        method: 'get'
    }).then((res) => {
        
        allCakesData=res.data.data;
        console.log("All Cake response====>", allCakesData);
        // setingCakesData(allCakesData);
        // allCakesData = res.data;
    }, (err) => {
        console.log("Error from all cakes", err);
    })

    



    // console.log("Cake data I got is====>", data);
    let handleChange = (e) => {
        setSearch(e.target.value);
        if (e.target.value.toString() != '') {
            setingCakesData(data);
        }

    }
    useEffect(() => {
        console.log('I am calling on searchstring change', searchString);
        // cakeData = cakeData.filter(item => item.name == searchString)
    }, [searchString]);

    let setCake = () => {
        let newcackesData = cackesData.filter(item => item.name.toString() == searchString.toString())
        console.log("New Cake Data-->", newcackesData)
        setingCakesData(newcackesData);
    }



    return (
        <div>
            <input onChange={handleChange} type="text" />
            <button onClick={setCake} className="btn btn-success">Search</button>
            <div className="row">

                {cackesData.map((each) => {
                    return <Cake key={each.cakeid} data={each} />
                })}
            </div>

        </div>
    )
}
export default Saerch;