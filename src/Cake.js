let Cake = (props) => {
  
    if (!props.data) {
        return null;
    }

    return (
        <div className="card" style={{ width: '18rem' }} >
            <img src={props.data.image} className="card-img-top" alt="..." />
            <div className="card-body">
                <h5 className="card-title">{props.data.name}</h5>
                <p className="card-text">{props.data.price}</p>
                {/* <a href="#" className="btn btn-primary">{props.data.option}</a> */}
                {/* {props.data.discount && <p>Discount:{props.data.discount}</p>}  */}
            </div>
        </div>
    )
}
export default Cake;