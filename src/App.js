// import logo from './logo.svg';
import Navbar from './Navbar'
// import Spot from "./Spot";
import Login from './Login'
// import SignUp from './Signup'
import Saerch from "./Search";
import Register from './Register';
// import { Carousel } from "./Carousel";
import Home from './Home';
import { BrowserRouter as Router, Redirect, Route } from 'react-router-dom';

import './App.css';


let spotArr = [{
  img: 'caro1.jpg',
  title: 'Test Spot',
  description: 'Test spot is awesome, you can visit here',
  option: 'Know More',
  discount: '50%'
},
{
  img: 'caro1.jpg',
  title: 'Test Spot',
  description: 'Test spot is awesome, you can visit here',
  option: 'Know More',
  discount: '50%'
},
{
  img: 'caro1.jpg',
  title: 'Test Spot',
  description: 'Test spot is awesome, you can visit here',
  option: 'Know More',
  discount: '50%'
},
{
  img: 'caro1.jpg',
  title: 'Test Spot',
  description: 'Test spot is awesome, you can visit here',
  option: 'Know More'
},
{
  img: 'caro1.jpg',
  title: 'Test Spot',
  description: 'Test spot is awesome, you can visit here',
  option: 'Know More'
}
]
function App() {
  console.log("env--->",process.env);
  //   let spotObj1={
  //     img:'caro1.jpg',
  //     title:'Test Spot',
  //     description:'Test spot is awesome, you can visit here',
  //     option:'Know More',
  //     discount:'50%'
  // }
  // let spotObj2={
  //   img:'caro1.jpg',
  //   title:'Test Spot',
  //   description:'Test spot is awesome, you can visit here',
  //   option:'Know More'
  // }

  // let preventfromCopy = (event) => {
  //   alert("Stop doing that");
  //   event.preventDefault();

  // }
  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}

      <h1>Welcome to My First React App</h1>
      {/* <Carousel /> */}
      {/* <div className="row"> */}
      {/* <input onCopy={preventfromCopy}></input> */}
      {/* <Spot data={spotObj} test="Hello"/>  to test changing props object value example:test*/}
      {/* <Spot data={spotObj1} test="Hello"/>
        <Spot data={spotObj1}/>
        <Spot data={spotObj1}/>
        <Spot data={spotObj1}/>
        <Spot data={spotObj2}/>
        <Spot data={spotObj2}/>
        <Spot data={spotObj2}/>
        <Spot data={spotObj2}/>
        <Spot data={spotObj2} />
        <Spot data={spotObj2}/>
        <Spot />
        <Spot /> */}

      {/* {spotArr.map((each) => {
          return <Spot data={each} />
        })}
      </div> */}
      {/* <Login /> */}
      {/* <Register /> */}
      {/* <SignUp></SignUp> */}
      {/* <Saerch /> */}
      <div>
        <Router>
          <Navbar>NatureLove</Navbar>
          <Route path="/" exact component={Home} />
          <Route path="/login" exact component={Login} />
          <Route path="/signup" exact component={Register} />
          <Route path="/search" exact component={Saerch} />
          <Route path="**"><Redirect to="/"></Redirect></Route>

        </Router>
      </div>
    </div>
  );
}

export default App;
