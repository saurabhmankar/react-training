
let Spot = (props) => {
    // props.data.title="Change Title"; //ineer object is mutable
    // props.test="Hii"; //props object cannot be change
    if (!props.data) {
        return null;
    }

    return (
        <div className="card" style={{ width: '18rem' }}>
            <img src={props.data.img} className="card-img-top" alt="..." />
            <div className="card-body">
                <h5 className="card-title">{props.data.title}</h5>
                <p className="card-text">{props.data.description}</p>
                <a href="#" className="btn btn-primary">{props.data.option}</a>
                {props.data.discount && <p>Discount:{props.data.discount}</p>} 
            </div>
        </div>
    )
}
export default Spot;