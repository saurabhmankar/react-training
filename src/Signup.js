import React  from 'react';

class SignUp extends React.Component {
    constructor() {
        super()
        this.state = {
            user: 0,
            isLoad:false
        }
    }

    joinMeeting=()=>{
        this.setState({
            isLoad:true
        })
        setTimeout(()=>{
            this.setState({
                user:this.state.user+1
            })
            this.setState({
                isLoad:false
            })
        },5000)
       
    }
    render() {
        return (
            <div>
                <button onClick={this.joinMeeting} className="btn btn-primary">Join Meeting</button>
                <p>{this.state.user} Join the Meeting</p>
                {this.state.isLoad && <h1>Loading Please Wait...</h1>}
            </div>
            
            
        )
    }
}
export default SignUp;