export const apiURL = {
    LOGIN: 'https://apifromashu.herokuapp.com/api/login',
    REGISTER: 'https://apifromashu.herokuapp.com/api/register',
    GETALLCAKES:'https://apifromashu.herokuapp.com/api/allcakes'
}