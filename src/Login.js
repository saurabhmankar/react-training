import axios from 'axios'
import { apiURL } from "./ApiConstantUrl";
let Login = () => {
    let user = {};
    let error = false;

    // let apiurl = "https://apifromashu.herokuapp.com/api/login";

    // let userCreds = {
    //     email: 'saurabh@gmail.com',
    //     password: 'Password@sm01'
    // }
    function handleEmail(e) {
        console.log("Username is--->", e.target.value);
        user.email = e.target.value;
    }
    function handlePassword(e) {
        console.log("Password is--->", e.target.value);
        user.password = e.target.value;
    }

    function login() {
        console.log("Enter Credentials are--->", user)
        axios({
            url: apiURL.LOGIN,
            method: 'post',
            data: user
        }).then((response) => {
            console.log("Response from Login", response);
        }, (error) => {
            console.log("Error from login", error);
        })
        // if (user.email != userCreds.email && user.password != userCreds.password) {
        //     error = true;
        //     console.log("Error true")
        // }

    }

    return (
        <div className="container">
            <div className="form-group">
                <label for="uname"><b>Username</b></label>
                <input onChange={handleEmail} className="form-control form-control-sm" type="text" placeholder="Enter Username" />

                <label for="psw"><b>Password</b></label>
                <input onChange={handlePassword} className="form-control form-control-sm" type="password" placeholder="Enter Password" />
                <br/>
                    <button onClick={login} type="submit" className="btn btn-info">Login</button>
                    {error && <p>Wrong Credetials are entered</p>}
                    <div>
                    </div>

            </div>
            </div>
    )
}
export default Login;


