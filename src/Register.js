import axios  from 'axios'
import { apiURL } from "./ApiConstantUrl";
let Register = () => {
    let user = {};
    let error = false;

    // let apiurl = "https://apifromashu.herokuapp.com/api/register";

    
    function handleEmail(e) {
        console.log("email is--->", e.target.value);
        user.email = e.target.value;
    }

    function handleName(e) {
        console.log("Username is--->", e.target.value);
        user.name = e.target.value;
    }
    function handlePassword(e) {
        console.log("Password is--->", e.target.value);
        user.password = e.target.value;
    }

    function register() {
        // console.log("Enter Credentials are--->", user)
        axios({
            url: apiURL.REGISTER,
            method: 'post',
            data: user
        }).then((response) => {
            console.log("Response from Login", response);
        }, (error) => {
            console.log("Error from login", error);
        })
        

    }

    return (
        <div className="container">
            <label for="uname"><b>Username</b></label>
            <input onChange={handleName} className="form-input" type="text" placeholder="Enter Username" />

            <label for="uname"><b>Email</b></label>
            <input onChange={handleEmail} className="form-input" type="text" placeholder="Enter Email" />

            <label for="psw"><b>Password</b></label>
            <input onChange={handlePassword} className="form-input" type="password" placeholder="Enter Password" />

            <button onClick={register} type="submit" className="btn btn-success">Register</button>
            {/* {error && <p>Wrong Credetials are entered</p>} */}
        </div>
    )
}
export default Register;


