let img1 = 'caro1.jpg';
let img2 = 'caro2.jpg';
let img3 = 'caro3.jpg';

export function Carousel() {
    return (
        <div id="carouselExampleControls" className="carousel slide" data-bs-ride="carousel">
            <div className="carousel-inner">
                <div className="carousel-item active">
                    <img src={img1} style={{height:'400px'}} className="d-block w-100" alt="..." />
                </div>
                <div className="carousel-item">
                    <img  src={img2} style={{height:'400px'}} className="d-block w-100" alt="..." />
                </div>
                <div className="carousel-item">
                    <img src={img3} style={{height:'400px'}}  className="d-block w-100" alt="..." />
                </div>
            </div>
            <button className="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Previous</span>
            </button>
            <button className="carousel-control-next" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                <span className="carousel-control-next-icon" aria-hidden="true"></span>
                <span className="visually-hidden">Next</span>
            </button>
        </div>
    )
}